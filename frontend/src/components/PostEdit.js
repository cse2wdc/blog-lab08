const React = require('react');
const _ = require('lodash');

/**
 * A form for editing a blog post.
 */
const PostEdit = React.createClass({
  displayName: 'PostEdit',
  getInitialState: function() {
    const post = this.props.post || {};
    return {
      title: post.title || '',
      content: post.content || ''
    };
  },
  revertAndStopEditing: function(event) {
    event.preventDefault();
    this.props.onCancel();
  },
  submitAndStopEditing: function(event) {
    event.preventDefault();
    // Creates a new post object and saves it.
    const editedPost = _.assign({}, this.props.post, {
      title: this.state.title,
      content: this.state.content
    });
    this.props.onSave(editedPost);
  },
  onTitleChange: function(event) {
    const title = event.target.value;
    this.setState({ title });
  },
  onContentChange: function(event) {
    const content = event.target.value;
    this.setState({ content });
  },
  render: function() {
    return (
      <form className="blog-post">
        {/* Title field */}
        <div className="form-group">
          <input className="form-control input-lg" value={this.state.title}
            placeholder="Post title" onChange={this.onTitleChange}
          />
        </div>
        {/* Content field */}
        <div className="form-group">
          <textarea
            className="form-control"
            style={{ height: 300 }}
            value={this.state.content}
            onChange={this.onContentChange}
          />
        </div>
        {/* Save button */}
        <button className="btn btn-default pull-right"
          onClick={this.submitAndStopEditing}
        >
          Save
        </button>
        {/* Cancel button */}
        <button className="btn btn-default pull-right"
          style={{ marginRight: '12px' }}
          onClick={this.revertAndStopEditing}
        >
          Cancel
        </button>
      </form>
    );
  }
});

module.exports = PostEdit;
