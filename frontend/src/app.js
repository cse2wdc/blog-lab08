const path = require('path');
const express = require('express');
const React = require('react');
const ReactDOMServer = require('react-dom/server');
const _ = require('lodash');
const moment = require('moment');

const api = require('./helpers/api');
const createStore = require('./helpers/createStore');
const Root = React.createFactory(require('./components/Root'));
const combinedReducers = require('./reducers');

// Create a new Express app
const app = express();

// Serve up our static assets from 'dist'
app.use('/assets', express.static(path.join(__dirname,
        '..', 'dist')));

// Serve up font-awesome fonts
app.use('/assets/font-awesome/fonts', express.static(
  path.dirname(require.resolve('font-awesome/fonts/FontAwesome.otf'))
));

// Set up the root route
app.get('/', (req, res) => {

// TODO Task 5: Uncomment this when you want to get the data from the api.

/* api.get('/posts').then((posts) => {
    try {
*/

    // TODO Task 5: Change code below to get data from the API
    // TODO Task 8: Change code below to use universal JavaScript


        const htmlContent = `
        <!DOCTYPE html>
        <html lang="en">
          <head>
            <title>My blog</title>
            <link rel="stylesheet" type="text/css" href="/assets/css/app.css">
             <script src="/assets/js/vendor.js"></script>
          </head>
          <body>
            <div class="container" id="root"></div>
            <script src="/assets/js/app.js"></script>
          </body>
        </html>`;

      // Respond with the complete HTML page
      res.send(htmlContent);

 // TODO Task 5: Uncomment this when you want to get the data from the api.
   /*
   }
    catch(err) {
      console.error(err.stack);
      res.status(500).send(err.stack);
    }

  }).catch((err) => {
    console.error(err.stack);
    res.status(500).send('Request to API failed.');
  }); */
});

module.exports = app;
